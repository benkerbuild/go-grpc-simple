package main

import (

	"log"
	"net"
	grpc "google.golang.org/grpc"

	"github.com/Benker-Leung/go-grpc-test/proto"
	"github.com/Benker-Leung/go-grpc-test/lib"

)


func main() {

	// listen to port
	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalf("cannot listen to port, error details [%v]", err)
	}

	grpcServer := grpc.NewServer()
	simple_service.RegisterSimpleServiceServer(grpcServer, &lib.GrpcService{})
	if err := grpcServer.Serve(lis); err != nil {
		log.Fatal("cannot serve grpc service")
	}

}
