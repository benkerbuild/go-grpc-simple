package lib

import(
	"os"
	"log"
	"context"
	
	"github.com/Benker-Leung/go-grpc-test/proto"
)

// GrpcService grpc server definition
type GrpcService struct {}

// GetService the grpc function implementation
func (s *GrpcService) GetService(ctx context.Context, req *simple_service.Request) (*simple_service.Reply, error) {

	var err error

	log.Printf("Received msg: [%v]", req.GetMessage())

	reply := &simple_service.Reply{}
	reply.InstanceName, err = os.Hostname()
	if err != nil {
		reply.InstanceName = "Unknown"
	}
	reply.Messge = "Simply return hostname"

	return reply, nil

}

