package main

import (

	grpc "google.golang.org/grpc"

	"log"
	"context"

	simple_service "github.com/Benker-Leung/go-grpc-test/proto"

)

func main() {

	// create connection
	conn, err := grpc.Dial("localhost:8080", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("fail to connect to grpc service")
	}
	defer conn.Close()

	// prepare the request
	req := &simple_service.Request{ Message: "simple call" }

	client := simple_service.NewSimpleServiceClient(conn)
	res, err := client.GetService(context.Background(), req)
	if err != nil {
		log.Fatalf("cannot call grpc service, error detail [%v]", err)
	}
	log.Printf("response instance name: [%v]", res.GetInstanceName())


}